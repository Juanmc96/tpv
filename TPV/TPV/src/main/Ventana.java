package main;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

public class Ventana extends JFrame{
	
	public Ventana() {
		
		this.setTitle("Traductor");
		
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	
	
		Toolkit tool = Toolkit.getDefaultToolkit();
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        this.setSize(screenSize.width, screenSize.height);

        this.setUndecorated(true);
		
		
		Layout layout = new Layout();
		
		this.add(layout);
		this.setVisible(true);
		
	}

}
