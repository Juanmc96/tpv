package main;

import java.awt.Button;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class Layout extends JPanel implements ActionListener {

	private Marco cont1;
	private Marco cont2;
	private Marco cont3;
	private Marco cont4;
	private Marco cont5;
	
	
	private JButton boton1;
	private JButton boton2;
	private JButton boton3;
	private JButton boton4;
	private JButton boton5;
	private JButton boton6;
	private JButton boton7;
	private JButton boton8;
	private JButton boton9;
	private JButton boton10;
	private JButton boton11;
	private JButton boton12;
	
	private JButton botonc1;
	private JButton botonc2;
	private JButton botonc3;
	private JButton botonc4;
	private JButton botonc5;
	private JButton botonc6;
	private JButton botonc7;
	private JButton botonc8;
	private JButton botonc9;
	private JButton botonc10;
	private JButton botonc11;
	private JButton botonc12;
	
	private ImageIcon croqueta;
	private Icon iconoCroqueta;
	
	private JTextArea text1;
	private TextArea text2;
	private JTextArea text3;
	private JTextArea text4;
	private JTextArea text5;
	
	private GridBagLayout grid;
	private GridBagConstraints config;


	public Layout() {

		grid = new GridBagLayout();
		config = new GridBagConstraints();
		

		this.setLayout(grid);

	
		cont1 = new Marco();
		cont2 = new Marco();
		cont3 = new Marco();
		cont4 = new Marco();
		cont5 = new Marco();

		GridBagConstraints config = new GridBagConstraints();

		config.gridx = 0;
		config.gridy = 2;
		config.fill = GridBagConstraints.BOTH;
		config.insets = new Insets(0, 0, 0, 0);
		config.weightx=1.0;
		config.weighty=1.0;
		

		this.add(cont1, config);

		config.gridx = 1;
		config.gridy = 2;


		this.add(cont2, config);

		config.gridx = 0;
		config.gridy = 1;
	

		this.add(cont3, config);
		
		config.gridx = 1;
		config.gridy = 1;
	

		this.add(cont4, config);
		
		config.gridx = 0;
		config.gridy = 0;
	

		this.add(cont5, config);
		
		boton1 = new JButton(new ImageIcon("agua.jpg"));
		boton1.addActionListener((ActionListener) this);

		config.gridx = 0;
		config.gridy = 0;

		
		cont1.add(boton1, config);
		
		boton2 = new JButton(new ImageIcon("bocata.jpg"));
		boton2.addActionListener((ActionListener) this);
		
		
		config.gridx = 0;
		config.gridy = 1;

	
		cont1.add(boton2, config);
		
		boton3 = new JButton(new ImageIcon("bravas.png"));
		boton3.addActionListener((ActionListener) this);

		config.gridx = 0;
		config.gridy = 2;

		
		cont1.add(boton3, config);
		
		boton4 = new JButton(new ImageIcon("cafe.jpg"));
		boton4.addActionListener((ActionListener) this);

		config.gridx = 1;
		config.gridy = 0;

		
		cont1.add(boton4, config);

		boton5 = new JButton(new ImageIcon("calamares.png"));
		boton5.addActionListener((ActionListener) this);

		config.gridx = 1;
		config.gridy = 1;

		
		cont1.add(boton5, config);
		
		boton6 = new JButton(new ImageIcon("cerveza.png"));
		boton6.addActionListener((ActionListener) this);

		config.gridx = 1;
		config.gridy = 2;

		
		cont1.add(boton6, config);
		
		boton7 = new JButton(new ImageIcon("cocacola.jpg"));
		boton7.addActionListener((ActionListener) this);

		config.gridx = 2;
		config.gridy = 0;

		
		cont1.add(boton7, config);
		
		boton8 = new JButton(new ImageIcon("croqueta.jpg"));
		boton8.addActionListener((ActionListener) this);

		config.gridx = 2;
		config.gridy = 1;

		
		cont1.add(boton8, config);
		
		boton9 = new JButton(new ImageIcon("hamburguesa.png"));
		boton9.addActionListener((ActionListener) this);

		config.gridx = 2;
		config.gridy = 2;

		
		cont1.add(boton9, config);
		
		boton10 = new JButton(new ImageIcon("huevo.jpg"));
		boton10.addActionListener((ActionListener) this);

		config.gridx = 3;
		config.gridy = 0;

		
		cont1.add(boton10, config);
		
		boton11 = new JButton(new ImageIcon("macarrones.png"));
		boton11.addActionListener((ActionListener) this);

		config.gridx = 3;
		config.gridy = 1;

		
		cont1.add(boton11, config);
		
		boton12 = new JButton(new ImageIcon("vino.png"));
		boton12.addActionListener((ActionListener) this);

		config.gridx = 3;
		config.gridy = 2;

		
		cont1.add(boton12, config);
		
		//--------------------------------------------------------------------
		
		text1 = new JTextArea("");
		text1.setFont(new Font("Arial", Font.BOLD, 36));
		text1.setBackground(Color.gray);

		config.gridx = 0;
		config.gridy = 0;
		config.gridwidth = 3;
		config.gridheight = 1;
		
		cont2.add(text1, config);
		
		botonc1 = new JButton("1");
		botonc1.addActionListener((ActionListener) this);
		
		config.gridwidth = 1;
		config.gridheight = 1;
		config.gridx = 0;
		config.gridy = 1;

		
		cont2.add(botonc1, config);
		
		botonc2 = new JButton("2");
		botonc2.addActionListener((ActionListener) this);

		config.gridx = 1;
		config.gridy = 1;

		
		cont2.add(botonc2, config);
		
		botonc3 = new JButton("3");
		botonc3.addActionListener((ActionListener) this);

		config.gridx = 2;
		config.gridy = 1;

		
		cont2.add(botonc3, config);
		
		botonc4 = new JButton("4");
		botonc4.addActionListener((ActionListener) this);

		config.gridx = 0;
		config.gridy = 2;

		
		cont2.add(botonc4, config);
		
		botonc5 = new JButton("5");
		botonc5.addActionListener((ActionListener) this);

		config.gridx = 1;
		config.gridy = 2;

		
		cont2.add(botonc5, config);
		
		botonc6 = new JButton("6");
		botonc6.addActionListener((ActionListener) this);

		config.gridx = 2;
		config.gridy = 2;

		
		cont2.add(botonc6, config);
		
		botonc7 = new JButton("7");
		botonc7.addActionListener((ActionListener) this);

		config.gridx = 0;
		config.gridy = 3;

		
		cont2.add(botonc7, config);

		botonc8 = new JButton("8");
		botonc8.addActionListener((ActionListener) this);

		config.gridx = 1;
		config.gridy = 3;

		
		cont2.add(botonc8, config);
		
		botonc9 = new JButton("9");
		botonc9.addActionListener((ActionListener) this);

		config.gridx = 2;
		config.gridy = 3;

		
		cont2.add(botonc9, config);
		
		botonc10 = new JButton("0");
		botonc10.addActionListener((ActionListener) this);

		config.gridx = 0;
		config.gridy = 4;

		
		cont2.add(botonc10, config);
		
		botonc11 = new JButton("NUEVO");
		botonc11.addActionListener((ActionListener) this);

		config.gridx = 1;
		config.gridy = 4;

		
		cont2.add(botonc11, config);
		
		botonc12 = new JButton("SALIR");
		botonc12.addActionListener((ActionListener) this);

		config.gridx = 2;
		config.gridy = 4;

		
		cont2.add(botonc12, config);
		
		//--------------------------------------------------------------------------
		
		text2 = new TextArea();
		
		

		config.gridx = 0;
		config.gridy = 0;
		text2.setFont(new Font("Arial", Font.BOLD, 18));
		
		cont3.add(text2, config);
		
		text3 = new JTextArea("0");
		
		
		text3.setFont(new Font("Arial", Font.BOLD, 36));

		config.gridx = 0;
		config.gridy = 0;
		
		cont4.add(text3, config);
		
		text4 = new JTextArea("0");
		
		text4.setFont(new Font("Arial", Font.BOLD, 36));
	

		config.gridx = 1;
		config.gridy = 0;
		
		cont5.add(text4, config);
		
		text5 = new JTextArea("TOTAL:           ");
		text5.setFont(new Font("Arial", Font.BOLD, 36));
	
		
		config.gridx = 0;
		config.gridy = 0;
		
		cont5.add(text5, config);
		
		
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		float suma=0;
		int sumatorio;
		
		if(e.getSource() == boton1){
			int cant;
			if(text1.getText().equalsIgnoreCase("")){
				cant=1;
			}else{
			cant=Integer.parseInt(text1.getText());
			}
			sumatorio=Integer.parseInt(text4.getText())+cant;
			text4.setText(""+sumatorio);
			
			
			suma=suma+(1*cant);
 
			TextArea valor= text2;
			text2.setText(valor.getText() + cant + "     Agua  1�   ->     " + suma + "�" + "\n");
			text1.setText("");
			double total=Double.parseDouble(text3.getText());
			total=total+suma;
			text3.setText(""+total);
		
		}
		if(e.getSource() == boton2){
			int cant;
			if(text1.getText().equalsIgnoreCase("")){
				cant=1;
			}else{
			cant=Integer.parseInt(text1.getText());
			}
			
			sumatorio=Integer.parseInt(text4.getText())+cant;
			text4.setText(""+sumatorio);
			
			suma=(float) (suma+(2.5*cant));
 
			TextArea valor= text2;
			text2.setText(valor.getText() + cant + "     Bocata  2.5�   ->     " + suma + "�" + "\n");
			text1.setText("");	
			double total=Double.parseDouble(text3.getText());
			total=total+suma;
			text3.setText(""+total);
		}
		if(e.getSource() == boton3){
			int cant;
			if(text1.getText().equalsIgnoreCase("")){
				cant=1;
			}else{
			cant=Integer.parseInt(text1.getText());
			}
			
			sumatorio=Integer.parseInt(text4.getText())+cant;
			text4.setText(""+sumatorio);
			
			suma=(float) (suma+(2.5*cant));
 
			TextArea valor= text2;
			text2.setText(valor.getText() + cant + "     Bravas  2.50�   ->     " + suma + "�" + "\n");
			text1.setText("");	
			double total=Double.parseDouble(text3.getText());
			total=total+suma;
			text3.setText(""+total);
		}
		if(e.getSource() == boton4){
			int cant;
			if(text1.getText().equalsIgnoreCase("")){
				cant=1;
			}else{
			cant=Integer.parseInt(text1.getText());
			}
			
			sumatorio=Integer.parseInt(text4.getText())+cant;
			text4.setText(""+sumatorio);
			
			suma=(float) (suma+(1*cant));
 
			TextArea valor= text2;
			text2.setText(valor.getText() + cant + "     Cafe  1�   ->     " + suma + "�" + "\n");
			text1.setText("");	
			double total=Double.parseDouble(text3.getText());
			total=total+suma;
			text3.setText(""+total);
		}
		if(e.getSource() == boton5){
			int cant;
			if(text1.getText().equalsIgnoreCase("")){
				cant=1;
			}else{
			cant=Integer.parseInt(text1.getText());
			}
			
			sumatorio=Integer.parseInt(text4.getText())+cant;
			text4.setText(""+sumatorio);
			
			suma=(float) (suma+(3.5*cant));
 
			TextArea valor= text2;
			text2.setText(valor.getText() + cant + "     Calamares  3.50�   ->     " + suma + "�" + "\n");
			text1.setText("");		
			double total=Double.parseDouble(text3.getText());
			total=total+suma;
			text3.setText(""+total);
		}
		if(e.getSource() == boton6){
			int cant;
			if(text1.getText().equalsIgnoreCase("")){
				cant=1;
			}else{
			cant=Integer.parseInt(text1.getText());
			}
			
			sumatorio=Integer.parseInt(text4.getText())+cant;
			text4.setText(""+sumatorio);
			
			suma=(float) (suma+(2*cant));
 
			TextArea valor= text2;
			text2.setText(valor.getText() + cant + "     Cerveza  2�   ->     " + suma + "�" + "\n");
			text1.setText("");	
			double total=Double.parseDouble(text3.getText());
			total=total+suma;
			text3.setText(""+total);
		}
		if(e.getSource() == boton7){
			int cant;
			if(text1.getText().equalsIgnoreCase("")){
				cant=1;
			}else{
			cant=Integer.parseInt(text1.getText());
			}
			
			sumatorio=Integer.parseInt(text4.getText())+cant;
			text4.setText(""+sumatorio);
			
			suma=(float) (suma+(1.5*cant));
 
			TextArea valor= text2;
			text2.setText(valor.getText() + cant + "     Coca cola  1.50�   ->     " + suma + "�" + "\n");
			text1.setText("");	
			double total=Double.parseDouble(text3.getText());
			total=total+suma;
			text3.setText(""+total);
		}
		if(e.getSource() == boton8){
			int cant;
			if(text1.getText().equalsIgnoreCase("")){
				cant=1;
			}else{
			cant=Integer.parseInt(text1.getText());
			}
			
			sumatorio=Integer.parseInt(text4.getText())+cant;
			text4.setText(""+sumatorio);
			
			suma=(float) (suma+(1.5*cant));
 
			TextArea valor= text2;
			text2.setText(valor.getText() + cant + "     Croqueta  1.50�   ->     " + suma + "�" + "\n");
			text1.setText("");		
			double total=Double.parseDouble(text3.getText());
			total=total+suma;
			text3.setText(""+total);
		}
		if(e.getSource() == boton9){
			int cant;
			if(text1.getText().equalsIgnoreCase("")){
				cant=1;
			}else{
			cant=Integer.parseInt(text1.getText());
			}
			
			sumatorio=Integer.parseInt(text4.getText())+cant;
			text4.setText(""+sumatorio);
			
			suma=(float) (suma+(5.5*cant));
 
			TextArea valor= text2;
			text2.setText(valor.getText() + cant + "     Hamburguesa  5.50�   ->     " + suma + "�" + "\n");
			text1.setText("");		
			double total=Double.parseDouble(text3.getText());
			total=total+suma;
			text3.setText(""+total);
		}
		if(e.getSource() == boton10){
			int cant;
			if(text1.getText().equalsIgnoreCase("")){
				cant=1;
			}else{
			cant=Integer.parseInt(text1.getText());
			}
			
			sumatorio=Integer.parseInt(text4.getText())+cant;
			text4.setText(""+sumatorio);
			
			suma=(float) (suma+(3.5*cant));
 
			TextArea valor= text2;
			text2.setText(valor.getText() + cant + "     Huevos  3.50�   ->     " + suma + "�" + "\n");
			text1.setText("");		
			double total=Double.parseDouble(text3.getText());
			total=total+suma;
			text3.setText(""+total);
		}
		if(e.getSource() == boton11){
			int cant;
			if(text1.getText().equalsIgnoreCase("")){
				cant=1;
			}else{
			cant=Integer.parseInt(text1.getText());
			}
			
			sumatorio=Integer.parseInt(text4.getText())+cant;
			text4.setText(""+sumatorio);
			
			suma=(float) (suma+(5*cant));
 
			TextArea valor= text2;
			text2.setText(valor.getText() + cant + "     Macarrones  5�   ->     " + suma + "�" + "\n");
			text1.setText("");		
			double total=Double.parseDouble(text3.getText());
			total=total+suma;
			text3.setText(""+total);
		}
		if(e.getSource() == boton12){
			int cant;
			if(text1.getText().equalsIgnoreCase("")){
				cant=1;
			}else{
			cant=Integer.parseInt(text1.getText());
			}

			sumatorio=Integer.parseInt(text4.getText())+cant;
			text4.setText(""+sumatorio);
			
			suma=(float) (suma+(2.5*cant));
 
			TextArea valor= text2;
			text2.setText(valor.getText() + text1.getText() + cant + "     Vino  2.50�   ->     " + suma + "�" + "\n");
			text1.setText("");	
			double total=Double.parseDouble(text3.getText());
			total=total+suma;
			text3.setText(""+total);
		}
		
		//------------------------------------------------------------------------------------------------
		
		if(e.getSource() == botonc1){
			JTextArea valor= text1;
			text1.setText(valor.getText() + "1");	
		}
		if(e.getSource() == botonc2){
			JTextArea valor= text1;
			text1.setText(valor.getText() + "2");	
		}
		if(e.getSource() == botonc3){
			JTextArea valor= text1;
			text1.setText(valor.getText() + "3");	
		}
		if(e.getSource() == botonc4){
			JTextArea valor= text1;
			text1.setText(valor.getText() + "4");	
		}
		if(e.getSource() == botonc5){
			JTextArea valor= text1;
			text1.setText(valor.getText() + "5");	
		}
		if(e.getSource() == botonc6){
			JTextArea valor= text1;
			text1.setText(valor.getText() + "6");	
		}
		if(e.getSource() == botonc7){
			JTextArea valor= text1;
			text1.setText(valor.getText() + "7");	
		}
		if(e.getSource() == botonc8){
			JTextArea valor= text1;
			text1.setText(valor.getText() + "8");	
		}
		if(e.getSource() == botonc9){
			JTextArea valor= text1;
			text1.setText(valor.getText() + "9");	
		}
		if(e.getSource() == botonc10){
			JTextArea valor= text1;
			text1.setText(valor.getText() + "0");	
		}
		if(e.getSource() == botonc11){
			text1.setText("");	
			text2.setText("");	
			text3.setText("0");
			text4.setText("0");
		}
		if(e.getSource() == botonc12){
			System.exit(0);	
		}
		
		
		
	}
	

	
}
